--[[ keys.lua ]]
local map = vim.api.nvim_set_keymap

-- remap the key used to leave insert mode
map('i', 'jk', '', {})
-- Easier split toggle

map('n', '<C-J>', '<C-W><C-J>', {})
map('n', '<C-K>', '<C-W><C-K>', {})
map('n', '<C-L>', '<C-W><C-L>', {})
map('n', '<C-H>', '<C-W><C-H>', {})

-- Disable arrows
map('n', '<Up>', '<Nop>', {})
map('n', '<Down>', '<Nop>', {})
map('n', '<Left>', '<Nop>', {})
map('n', '<Right>', '<Nop>', {})

-- Pasting yanked not deleted n' yanked value
map('n', ',p', '"0p', {})
map('n', ',P', '"0P', {})

-- If it's not under VSCode add those mappings
if vim.g.vscode == nil then

  lvim.lsp.on_attach_callback = function(_, bufnr)
    local bufopts = { noremap = true, silent = true, buffer = bufnr }
    vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
    vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
    vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
    vim.keymap.set('n', '<space>wl', function()
      print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, bufopts)
  end

  -- Lsp rename
  map("n", "<space>rn", "<cmd>lua vim.lsp.buf.rename()<CR>", { noremap = true })

  -- Rust hover actions
  map('n', '<C-Space>', [[:RustHoverActions<CR>:RustHoverActions<CR>]], {})

  -- Toggle nvim-tree
  map('n', 't', [[:NvimTreeToggle<CR>:NvimTreeRefresh<CR>]], {})

  -- Search for files easily
  map('n', '<C-P>', [[:Telescope find_files<CR>]], {})

  -- Code Formatting
  map('n', '<C-F>', [[:Prettier<CR>]], {})

  -- Jump to selected word
  map('n', '<C-Y>', [[:HopWord<CR>]], {})

  -- Switch between tabs
  map('n', 'gt', '[[:BufferNext<CR>]]', {})
  map('n', 'gT', '[[:BufferPrevious<CR>]]', {})
end
